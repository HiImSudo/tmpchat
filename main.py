#!/usr/bin/env python3

import json
from flask import Flask, request, Response
app = Flask(__name__)

from passport import generate_passport
from auth import validatePassport
from area import areaAuthenticate

import copy
import time

datastore = None
with open('./data/store.json', 'r') as fd:
    datastore = json.load(fd)

(nouns, adjectives) = (None, None)
with open('./data/random.json', 'r') as fd:
    values = json.load(fd)
    nouns = values['nouns']
    adjectives = values['adjectives']

print('Datastore loaded. . .')

# Error Handlers
@app.errorhandler(401)
def unauth_error(error):
    return Response(error, 401)

@app.errorhandler(500)
def server_error(error):
    return Response(error, 500)
"""
    Three endpoints all interacting with the datastore:
    
    - passport
    - message
    - area
    - debug
"""

# Passport
@app.route('/passport')
def getPassport():
    passport = generate_passport(nouns, adjectives)
    datastore['passports'].append(passport)
    return passport

# Message
@app.route('/createMessage', methods=['POST'])
def createMessage():
    content = request.get_json()
    import pprint
    pprint.pprint(content)
    if content != None and content.get("passport") != None:
        res = validatePassport(content['passport'], datastore['passports'])
        if res:
            if datastore['areas'].get(content['name']) != None:
                if areaAuthenticate(datastore['areas'][content['name']], content["passport"]["id"]):
                    # Add message 
                    now = time.time()
                    datastore['messages'][str(now)] = {
                        "area": content['name'],
                        "user": content['passport']['id'],
                        "data": content['message'],
                        "timestamp": time.time_ns()
                    }
                    return "200 OK"
                else:
                    return unauth_error('Error while sending message')
            return server_error("Error while adding message")
    return unauth_error('Invalid passport')
    
@app.route('/message', methods=['POST'])
def getMessage():
    content = request.get_json()
    if content != None and content.get("passport") != None:
        res = validatePassport(content['passport'], datastore['passports'])
        if res:
            messages = []
            for message in datastore['messages']:
                if datastore['messages'][message]['area'] == content['name']:
                    if content.get('timestamp') != None and datastore['messages'][message].get('timestamp') != None \
                            and content['timestamp'] > datastore['messages'][message]['timestamp']:
                        continue
                    data = copy.deepcopy(datastore['messages'][message])
                    
                    # Strip area information and append timestamps
                    del data['area']
                    data['timestamp'] = float(message)

                    messages.append(data)
            if len(messages) == 0:
                messages.append({
                    "user": "SYSTEM",
                    "data": "No messages yet",
                    "timestamp": 0.0
                    })
            return {"messages": messages}

# Area
@app.route('/createArea', methods=['POST'])
def createArea():
    content = request.get_json()
    if content != None and content.get("passport") != None:
        res = validatePassport(content['passport'], datastore['passports'])
        if res:
            datastore['areas'][content['name']] = {"users": [content['passport']['id']]}
            if content.get("password") != None:
                datastore['areas'][content['name']]['password'] = content['password']
            return "200 OK"

    return unauth_error('Invalid passport')
    
@app.route('/area', methods=['POST'])
def area():
    content = request.get_json()
    if content != None and content.get("passport") != None:
        res = validatePassport(content['passport'], datastore['passports'])
        if res:
            if datastore['areas'].get(content['name']) != None:
                if datastore['areas'][content['name']].get('password') != None:
                    # Password auth
                    if content['password'] == datastore['areas'][content['name']]['password']:
                        datastore['areas'][content['name']]['users'].append(content['passport']['id'])
                    else:
                        return unauth_error('Warhammer is pretty cool')
                else:
                        datastore['areas'][content['name']]['users'].append(content['passport']['id'])
                return "200 OK"
            else:
                    return unauth_error('Error while joining area')
            return server_error("Error while joining area")

    return unauth_error('Invalid passport')
    
# Token Test
@app.route('/test', methods=['POST'])
def tokenTest():
    content = request.get_json()
    if content != None and content.get("passport") != None:
        if validatePassport(content['passport'], datastore['passports']):
            return {"valid": True}
        else:
            return {"valid": False}
    return {"valid": False}

# Debug
@app.route('/debug')
def debug():
    return datastore

