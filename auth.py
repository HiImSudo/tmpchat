def validatePassport(passport: dict, passports: list) -> bool:
    for x in passports:
        if x['id'] == passport['id']:
            return x['token'] == passport['token']
    return False    