import random
import secrets
from Crypto.PublicKey import RSA
import base64


passport = {}
key = RSA.generate(2048)
private_key = key.exportKey(format="DER")
public_key = key.publickey().exportKey(format="PEM")


passport["private-key"] = base64.b64encode(private_key).decode()
passport['public-key'] = base64.b64encode(public_key).decode()
