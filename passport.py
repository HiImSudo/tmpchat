import random
import secrets
import base64


def generate_passport(nouns: list, adjectives: list, tokenLength: int = 64) -> dict:
    passport = {}

    passport["id"] = random.choice(adjectives) + random.choice(nouns)
    passport["token"] = secrets.token_hex(tokenLength)

    return passport
