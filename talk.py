#!/usr/bin/env python3

import requests
import json
import random
import time

(nouns, adjectives) = (None, None)
with open('./data/random.json', 'r') as fd:
    values = json.load(fd)
    nouns = values['nouns']
    adjectives = values['adjectives']

passport = requests.get("https://ioctl.xyz:8000/passport")
passport = json.loads(passport.content)

area = "debug"
password = ""

requests.post("https://ioctl.xyz:8000/area",json={"passport": passport, "name": area, "password": password})

while True:
    time.sleep(random.randrange(1, 4))
    requests.post("https://ioctl.xyz:8000/createMessage",json={"passport": passport, "message": "Hello " + random.choice(adjectives) + random.choice(nouns), "name": area})

