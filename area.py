def areaAuthenticate(area: dict, id: str) -> bool:
    return id in area['users']
        